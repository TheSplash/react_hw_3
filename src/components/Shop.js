import React from 'react'
import Cards from './Cards'
import '../style/shop.scss'

const Shop = ({handleClick, amount, toggleFavourites, favorites}) => {
  return (
    <section className="shop_container">
        {
            amount.map((item) => (
                <Cards item={item} key={item.id} 
                boolean={favorites.some((el) => {return el.id === item.id})} 
                handleClick={handleClick} 
                toggleFavourites={toggleFavourites}/>
            ))
        }
    </section>
  )
}

export default Shop

