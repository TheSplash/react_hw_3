import { Outlet } from "react-router-dom";
import Navbar  from "./Navbar";
import React from 'react'
import { useEffect, useState } from 'react';

const Layout = () => {
  const initialCart = JSON.parse(localStorage.getItem('cart')) || [];
  const [cart, setCart] = useState(initialCart)
  const [amount, setAmount] = useState([])
  const [favorites, setFavorites] = useState([])

  /* FAVOURITES */

  const toggleFavourites = (item) => {
    if (favorites.some((el) => el.id === item.id)) {
      const updatedFavorites = favorites.filter((el) => el.id !== item.id);
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    } else {
      const updatedFavorites = [...favorites, item];
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    }
  };

  useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem('favorites'));
    if (storedFavorites) {
      setFavorites(storedFavorites);
    }
  }, []);

  /* AJAX */

  useEffect(() => {
    fetch('./data.json')
      .then((response) => {
        return response.json();
      })
        .then((data) => {
          setAmount(data)
        });
  
  }, []) 

  /* CART */

   useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  const handleClick = (item) => {
    setCart((prev) => {
      const findEl = prev.find((product) => item.id === product.id);
      if (findEl) {
        findEl.count += 1
        return [...prev]
      } else {
        item.count = 1
        return [...prev, item]
      }
    });
  };

  const handleDelete = (itemId) => {
    setCart((prev) => {
      return prev.map((item) => {
        if (item.id === itemId) {
           item.count = Math.max(item.count - 1, 0);
          if (item.count === 0) {
            return null; 
          }
        }
        return item;
      }).filter(Boolean); 
    });
  };

  const cartSize = (cart) => {
    const mapingCart = cart.map((item) => {
      return item.count
    })

    if (mapingCart.length > 0) {
      const totalAmount = mapingCart.reduce(function (previousValue, currentValue) {
        return previousValue + currentValue
      })
      return totalAmount
    } else {
      return 0
    }
  } 
  
  return (
    <>
    <header>
        <Navbar cartSize={cartSize(cart)} favouritesSize={favorites.length}/>
    </header>

    <Outlet context={{
      handleClick,
      cart,
      amount,
      toggleFavourites,
      favorites,
      handleDelete
    }}/>
    </>
  )
}

export  {Layout}