import { useOutletContext } from "react-router";
import CardsDelete from "../components/CardsDelete";
import '../style/cart.scss'

const Cart = () => {
  const {cart, handleDelete, favorites, toggleFavourites} = useOutletContext()
  return (
    <article className="cart_container">
      {cart.map((item) => (
        <CardsDelete item={item} key={item.id} 
        boolean={favorites.some((el) => {return el.id === item.id})} 
        handleDelete={handleDelete} 
        toggleFavourites={toggleFavourites}
        />
      ))}
    </article>
  );
};

export {Cart};
