import { useOutletContext } from "react-router";
import Cards from "../components/Cards";

const Favourites = () => {
  const {handleClick, toggleFavourites, favorites} = useOutletContext()
  return (
    <article>
      {favorites.map((item) => (
        <Cards item={item} key={item.id} 
        boolean={favorites.some((el) => {return el.id === item.id})} 
        handleClick={handleClick} 
        toggleFavourites={toggleFavourites}/>
       
      ))}
    </article>
  )
}

export {Favourites}