import React from 'react'
import Shop from '../components/Shop';
import { useOutletContext } from 'react-router';

function HomePage() {
  const {cart,handleClick, amount, toggleFavourites, favorites, } = useOutletContext()
  

  return (
    <div className="App">
       <Shop 
          handleClick={handleClick} 
          cart={cart} 
          amount={amount} 
          toggleFavourites={toggleFavourites} 
          favorites={favorites}/>
      
    </div>
  );
}

export {HomePage}