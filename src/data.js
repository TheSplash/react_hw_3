const list = [
  {
    id: 1,
    name: "Final Offer",
    price: 50,
    image_link: "./img/image-1,jpg",
    color:"green"
  },
  {
    id: 2,
    name: "It ends with Us",
    price: 50,
    image_link: "img1.jpg",
    color:"pink"
  },
  {
    id: 3,
    name: "Atomic Habits",
    price: 50,
    image_link: "img1.jpg",
    color:"white"
  },
  {
    id: 4,
    name: "The creative Act",
    price: 50,
    image_link: "img1.jpg",
    color:"grey"
  },
  {
    id: 5,
    name: "Spare",
    price: 50,
    image_link: "img1.jpg",
    color:"beige"
  },
  {
    id: 6,
    name: "Digital minimalism",
    price: 50,
    image_link: "img1.jpg",
    color:"blue"
  },
  {
    id: 7,
    name: "Meditations",
    price: 50,
    image_link: "img1.jpg",
    color:"dark"
  },
  {
    id: 8,
    name: "How to think...",
    price: 50,
    image_link: "img1.jpg",
    color:"brown"
  },
  {
    id: 9,
    name: "Meditation of the Marcus",
    price: 50,
    image_link: "img1.jpg",
    color:"yellow"
  },
  {
    id: 10,
    name: "Lives of the...",
    price: 50,
    image_link: "img1.jpg",
    color:"red"
  },
  
];

export default list;